import pypdftk
import io
import pandas


csvname="csv/proj_bmt_28_auto_2019.csv"
data=pandas.read_csv(csvname)
year=csvname[-8:-4]

print(len(data.index)) 

for row in range(len(data.index)):
    lastname= data['Last_Name'][row]
    firstname= data['First_Name'][row]
    filename="{}_{}_{}.pdf".format(firstname,lastname,year)
 
    datas = {

    "CIC" : data['CIC'][row],
    "UPN" : data['UPN'][row],
    "DoB" : data["DoB"][row],
    "Day_0" : data["Day_0"][row],
    "First_Initial" : data["First_Initial"][row],
    "Second_Initial" : data["Second_Initial"][row],
    "Sex" : data["Sex"][row],
    "Weight_(Kg)" : data["Weight_(Kg)"][row],
    "Height_(cm)" : data["Height_(cm)"][row],
    "Karnofsky_score" : data["Karnofsky_score"][row],
    "Score" : data["Karnofsky_score"][row],
    "Number_of_HSCT" : data["Number_of_HSCT"][row],
    "Date_Initial_Diagnosis" : data["Date_Initial_Diagnosis"][row],
    "Date_Initial_Diagnosis_2" : data["Date_Initial_Diagnosis"][row],
    "CIC_1" : data['CIC'][row],
    "CIC_2" : data['CIC'][row],
    "CIC_3" : data['CIC'][row],
    "CIC_4" : data['CIC'][row],
    "CIC_5" : data['CIC'][row],
    "CIC_6" : data['CIC'][row],
    "CIC_2" : data['CIC'][row],
    "CIC_3" : data['CIC'][row],
    "CIC_4" : data['CIC'][row],
    "CIC_5" : data['CIC'][row],
    "CIC_6" : data['CIC'][row],
    "CIC_7" : data['CIC'][row],
    "CIC_8" : data['CIC'][row],
    "CIC_9" : data['CIC'][row],
    "CIC_10" : data['CIC'][row],
    "CIC_11" : data['CIC'][row],
    "UPN_1" : data['UPN'][row],
    "UPN_2" : data['UPN'][row],
    "UPN_3" : data['UPN'][row],
    "UPN_4" : data['UPN'][row],
    "UPN_5" : data['UPN'][row],
    "UPN_6" : data['UPN'][row],
    "UPN_7" : data['UPN'][row],
    "UPN_8" : data['UPN'][row],
    "UPN_9" : data['UPN'][row],
    "UPN_10" : data['UPN'][row],
    "UPN_11" : data['UPN'][row],
    "Date_1" : data['Day_0'][row],
    "Date_2" : data['Day_0'][row],
    "Date_3" : data['Day_0'][row],
    "Date_4" : data['Day_0'][row],
    "Date_5" : data['Day_0'][row],
    "Date_6" : data['Day_0'][row],
    "Date_7" : data['Day_0'][row],
    "Date_8" : data['Day_0'][row],
    "Date_9" : data['Day_0'][row],
    "Date_10" : data['Day_0'][row],
    "Date_11" : data['Day_0'][row],
    "Date_12" : data['Day_0'][row],

    }
    if data["Primary_Diagnosis"][row]=="C83.3":
        f=open("auto/{}/nhl/{}".format(year,filename),"w+")
        template="templates/auto/Non_Hodgkin_Lymphoma-B_Cell_Day_0_Autograft_MED-A_Form_0_temp.pdf"
        generated_pdf = pypdftk.fill_form(template, datas,"auto/{}/nhl/{}".format(year,filename))
    if data["Primary_Diagnosis"][row]=="C84.4":
        f=open("auto/{}/nhl/{}".format(year,filename),"w+")
        template="templates/auto/Non_Hodgkin_Lymphoma-T_Cell_Day_0_Autograft_MED-A_Form_temp.pdf"
        generated_pdf = pypdftk.fill_form(template, datas,"auto/{}/nhl/{}".format(year,filename))
    f.close()
