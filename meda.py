import pypdftk
import io
import pandas


csvname="csv/CIC-255.csv"
data=pandas.read_csv(csvname)


print(len(data.index)) 

for row in range(len(data.index)):
    lastname= data['Surname'][row]
    firstname= data['Name'][row]
    year=data['Treatment'][row]
    filename="{}_{}_{}.pdf".format(firstname,lastname,year)
    if data["Sex"][row]=="Male[1]":
        Male="On" 
        Female=""
    if data["Sex"][row]=="Female[2]":
        Female="On" 
        Male=""
    
    datas = {

    "PRIMARY DISEASE DIAGNOSIS" : data['Diagnosis'][row],
    "UPN1" : data['UPN'][row],
    "HSCT" : data['Treatment'][row],
    "Date_of_Birth" : data["DoB"][row],
    #"Date_last_follow_or_death#3" : data["Date Last Follow"][row],
    "First_Initial" : data["First_Initial"][row],
    "Last_Initial" : data["Last_Initial"][row],
    #"Date_recent_follow" : data["Date recent follow"][row],
    #"Date_first_seen" : data["Date_first_seen"][row],
    "Female" : Female,
    "Male" : Male,
    }

    if data["HSCT type"][row]=="Allogeneic[1]":
        f=open("meda/{}".format(filename),"w+")
        template="templates/med-a/Annual_Follow_Up_Allograft_MED-A_Form.pdf"
        generated_pdf = pypdftk.fill_form(template, datas,"meda/{}".format(filename))
    if data["HSCT type"][row]=="Autologous[2]":
        f=open("meda/{}".format(filename),"w+")
        template="templates/med-a/Annual_Follow_Up_Autograft_MED-A_Form.pdf"
        generated_pdf = pypdftk.fill_form(template, datas,"meda/{}".format(filename))
    f.close()
