# PDF Filling for Patient Forms

This python code will help you generate pdf files from csv data 

## Getting Started

You need to have any version of python installed to your computer first then you need to install two other libraries (pypdftk and pandas)
### Prerequisites

From command line go to your directory and install the libraries below

```cmd
pip install pypdftk
pip install pandas
```

### Installing

You need to put the csv file into templates. If there is any updated pdf template you need to update it also. Choose the type of your report(for med-a forms ) 

```cmd
python meda.py
```